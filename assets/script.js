const boxes = document.querySelectorAll(".box");

let result = 0;
let timer = 10;
let started = false;

const startGame = () => {
  started = true;
  timer = 10;
  result = 0;
  document.querySelector("#result").textContent = result;
  document.querySelector("#timer").textContent = timer;
  document.querySelector("#startButton").style.backgroundColor = "red";

  const timerInterval = setInterval(() => {
    document.querySelector("#timer").textContent = --timer;
    console.log(timer);
  }, 1000);

  const interval = setInterval(() => {
    document.querySelector(".mole").classList.remove("mole");

    const randomMole = Math.floor(Math.random() * 9);
    boxes[randomMole].classList.add("mole");
  }, 1000);

  setTimeout(() => {
    clearInterval(timerInterval);
    clearInterval(interval);
    alert(`Game is over! Your score is ${result}`);
    document.querySelector("#startButton").style.backgroundColor = "rgb(40, 24, 5)";

    started = false;
  }, 10100);

  boxes.forEach((box) => {
    box.addEventListener("click", () => {
      if (box.id == document.querySelector(".mole").id && timer > 0) {
        document.querySelector("#result").textContent = ++result;
      }
    });
  });
};

document.querySelector("#startButton").addEventListener("click", () => {
  if (!started) startGame();
  else alert("Game did not over!");
});
